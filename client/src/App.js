import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table, Spin } from 'antd';
import './App.css';
import 'antd/dist/antd.css';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: 'Market cap',
    dataIndex: 'marketCapUsd',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.marketCapUsd - b.marketCapUsd,
  },
  {
    title: 'Last price in USD',
    dataIndex: 'priceUsd',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => a.priceUsd - b.priceUsd,
  },
  {
    title: 'Last price in EUR',
    dataIndex: 'priceEur',
    sortDirections: ['descend', 'ascend'],
    sorter: (a, b) => a.priceEur - b.priceEur,
  }
];


function App() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios('/');
      setData(result.data);
      setLoading(false);
    };

    fetchData();
  }, []);

  return (
    <>
      <div className="topBar">
        CRYPTO PRICE WATCH
      </div>
      <div className="main">
        <ul>
          {loading ?
            <Spin tip="Loading..."/>
            :
            <Table columns={columns} dataSource={data} pagination={false} />
          }
        </ul>
      </div>
    </>
  );
}

export default App;
