# CryptoWatch

## Description
- Front End built with React using Ant Design components and axios for the api request.
    - It's a simple page that ask the backend for the crypto data and renders it on a table.
- Back end built with Node and Express using MongoDB and Mocha, Chai, Nock and supertest for testing.
    - It gets the data from the coincap api and the usd to eur rate from ratesapi.

## How to run
- Running it locally:

    - api: `npm start`
    - api tests: `npm test`
    - client: `yarn start`


- Using Docker Compose:
    ```
    docker-compose up
    ```
