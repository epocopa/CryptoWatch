const { expect } = require('chai')
const nock = require('nock');
const request = require('supertest');
const app = require('../app');

describe('Test API', () => {
    after(() => {
        nock.restore();
    });

    afterEach(() => {
        nock.cleanAll();
    });

    it('should return live data if the request works', async () => {
        const res = await request(app).get('/');
        expect(res.status).to.equal(200);
        expect(res.body.length).to.equal(10);
    });

    it('should return last saved data if the request fails', async () => {
        const scope = nock('https://api.coincap.io')
            .get(/.*/)
            .reply(500);

        const res = await request(app).get('/');
        expect(res.status).to.equal(200);
        expect(res.body.length).to.equal(10);
        scope.done();
    });
});