const express = require('express');
const axios = require('axios');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017';
const dbName = 'cryptowatch';

const getInfo = async () => await axios('https://api.coincap.io/v2/assets?limit=10')

const getRates = async () => await axios('https://api.ratesapi.io/api/latest?symbols=USD')

const addEUR = (info, rate) => {
  return info.map(e => ({
    name: e.name,
    priceEur: (e.priceUsd / rate).toFixed(2),
    priceUsd: parseFloat(e.priceUsd).toFixed(2),
    marketCapUsd: parseFloat(e.marketCapUsd).toFixed(2),
  }))
}
// We serve and save the data if there are no errors, 
//and if the request fails it will serve the latest info from the db
router.get('/', (req, res, next) => {
  MongoClient.connect(url, {
    useUnifiedTopology: true
  }, async (err, client) => {
    const db = client.db(dbName);
    try {
      const info = await getInfo();
      const rates = await getRates();
      const infoWithEUR = addEUR(info.data.data, rates.data.rates.USD);
      db.collection('prices').insertOne({infoWithEUR})
      res.send(infoWithEUR)
    } catch (error) {
      console.log("Network failed");
      db.collection('prices').find().sort( { _id : -1 } ).limit(1).toArray((err, results) => {
        res.send(results[0].infoWithEUR)
      })
    } 
    });
});

module.exports = router;
